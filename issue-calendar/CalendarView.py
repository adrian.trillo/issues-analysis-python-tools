'''
Created on 3 Nov 2021

@author: Adrian.Trillo
'''

from openpyxl import load_workbook
import re
from datetime import datetime



'''
Created on 18 Aug 2021

@author: Adrian.Trillo
'''

#https://www.reddit.com/r/learnpython/comments/53wph1/connecting_to_a_mysql_database_in_a_python_script/


import pymysql
import paramiko
import pandas as pd
from paramiko import SSHClient
from sshtunnel import SSHTunnelForwarder
from os.path import expanduser

from openpyxl import Workbook
import pandas as pd
from openpyxl.utils import get_column_letter
from openpyxl.styles import Color, PatternFill, Font, Border
from openpyxl.styles import colors
from getpass import getpass

import math
import os
home = expanduser('~')

sql_username = ''
sql_password = ''
query =''
excel_name='default'
#query = '''SELECT from_unixtime(chg_timestamp) FROM mywallbox.charger_log_status where charger_id = 137682 and (power_sharing_status =2 or power_sharing_status=4)'''

'******************************************************'
'            GET DATA FROM SQl                         '
'******************************************************'

def get_data_from_sql():
    
    mypkey = paramiko.RSAKey.from_private_key_file('operations.pem')
    
    
    sql_hostname = 'telemetry-prod-cluster.cluster-ro-cywx2ir9bobq.eu-central-1.rds.amazonaws.com'
    sql_main_database = 'mywallbox'
    sql_port = 3306
    ssh_host = '172.31.17.241'
    ssh_user = 'ubuntu'
    ssh_port = 22
    sql_ip = '1.1.1.1.1'
    
    with SSHTunnelForwarder(
            (ssh_host, ssh_port),
            ssh_username=ssh_user,
            ssh_pkey=mypkey,
            remote_bind_address=(sql_hostname, sql_port)) as tunnel:
        conn = pymysql.connect(host='127.0.0.1', user=sql_username,
                passwd=sql_password, db=sql_main_database,
                port=tunnel.local_bind_port)
    
    
        data = pd.read_sql_query(query, conn)
        conn.close()
        
    return data


'******************************************************'
'            VARIABLES                                 '
'******************************************************'

days = ['Monday','Tuesday','Wednesday','Thrusday','Friday', 'Satuday', 'Sunday']


finish_row = 0
start_column = 1
finish_col = 7
actual_row = 1
actual_column = 1
date = 0

redFill = PatternFill(start_color='FFFF0000',
                   end_color='FFFF0000',
                   fill_type='solid')

'******************************************************'
'            MAIN                                      '
'******************************************************'
#Get mail, password and query

sql_username = input("E-mail: ")
sql_password = getpass("Database Password: ")
query = input("Query to analyse: ")
excel_name = input("Excel Name: ")


#Get data from sql
data = get_data_from_sql()

#Create a workbook and use the first sheet
workbook = Workbook()
sheet = workbook.active

#get the first and last data of the sql
first_date = data.iat[0,0]
last_date = data.iat[-1,0]


#get the number of days between the first and the last 
delta = last_date - first_date
cant_days = delta.days

first_date = first_date.strftime("%Y-%m-%d")
last_date = last_date.strftime("%Y-%m-%d")
#Create a list with all days (maybe in the database there is not all days)
dt = pd.date_range(start = first_date, end= last_date, freq='D')

#day of the week of the first day (0-->lunes, 6-->Sunday)
first_day_of_the_week = dt[0].day_of_week

finish_row = math.ceil(cant_days/7)

data['from_unixtime(chg_timestamp)'] = data['from_unixtime(chg_timestamp)'].astype(str)

data['from_unixtime(chg_timestamp)'].astype(str).str.contains('2021-07-10').any()

'''
sheet.merge_cells('A'+str(actual_row)+':G'+str(actual_row))
actual_row +=1
for day in days:
    sheet.cell(row=actual_row,column=actual_column).value = day
    actual_column +=1    

actual_row +=1
'''
for values in sheet.iter_rows(min_row = actual_row, max_row =finish_row+1, values_only = True):
    for column in range(7):
        try:
            sheet.cell(row=actual_row, column=dt[date].day_of_week + 1).value = dt[date].strftime("%Y-%m-%d")
            Exist_error = data['from_unixtime(chg_timestamp)'].astype(str).str.contains(dt[date].strftime("%Y-%m-%d")).any()
            sheet.cell(row=actual_row+1, column=dt[date].day_of_week + 1).value = Exist_error
            if (Exist_error):
                sheet.cell(row=actual_row+1, column=dt[date].day_of_week + 1).fill = redFill
            date+=1
            if(dt[date].day_of_week)==0:
                break
            
        except:
            pass 
    actual_row+=2;

workbook.save(filename= excel_name + '.xlsx')

print("The file " + excel_name + '.xlsx is in ' + os.getcwd() )





