This program generates an excel to show the days where an issue appear

**Requeriments:**

1. Install python in your computer --> https://www.python.org/downloads/
2. Download or clone the program of this page.
2. Install the necessary libraries:
    
- Go to a terminal or cmd 
- Be sure that you have pip installed in your pc

> pip --version

- In the terminal or cmd go to the folder where you downloaded the file using 

> cd folder

- execute: 
   ` pip install -r requirements.txt`
4. Connect to the VPN
5. copy the file .pem of the database inside the program folder and recall it as operations.pem

**How to use it:**

1. Execute the program (you can make double clic on the .py file or by terminal with 
> python nombreDelPrograma.py
2. Write your mail (the one used for accessing the database)
3. Write the password that you use for accessing the database in the cloud
4. Write the query that you want to know the days that happend


- Conditions: 
- [ ] The **first columnn must be the date**
- [ ] The query must be order with: **order by chg_timestamp asc;**


- Example: This query shows the days that the charger 137682 unpairs from the powersharing --> 
    
    > SELECT from_unixtime(chg_timestamp) FROM mywallbox.charger_log_status where charger_id = 137682 and (power_sharing_status =2 or power_sharing_status=4) order by chg_timestamp asc;

5. In the folder where you download the program an excel file will be created.


